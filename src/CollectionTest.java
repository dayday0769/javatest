import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CollectionTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String str = "abs ab abs sv ab";
		String[] strs = str.split(" ");
		Map<String,Integer> map = new java.util.HashMap<String,Integer>();
		Set<String> set = new HashSet<String>();
		for(String str1:strs){
			if (!map.containsKey(str1)) {
				map.put(str1, 1);
			} else {
				int index = map.get(str1);
				map.remove(str1);
				map.put(str1, index+1);
			}
			set.add(str1);
		}
		Iterator<String> iterator = set.iterator();
		while (iterator.hasNext()) {
			String str2 = iterator.next();
			System.out.println(str2+"  "+map.get(str2));
		}
	}
}
