


/**
 * 快速排序
 * @author chenjianjun
 * @time 2014-11-14 下午5:53:38
 */
public class FastSort {
	
	public static void main(String[] args) {
		int[] lists = new int[]{34,3,53,2,23,7,14,10,23};
		_quick(lists,0,lists.length-1);
		for (int i = 0; i < lists.length; i++) {
			System.out.print(lists[i]+"\t");
		}
	}
	
	/**
	 * 排序
	 * 获取中间值，再分别对左右两边迭代排序
	 * @param lists
	 * @param low
	 * @param high
	 */
	public static void _quick(int[] lists,int low,int high){
		if (low<high) {
			int middle = getMiddle(lists,low,high);
			_quick(lists,low,middle-1);
			_quick(lists,middle+1,high);
		}
	}
	
	/**
	 * 获取下标中间值
	 * 大于key值放右边，小于key值放左边
	 * @param lists
	 * @param low
	 * @param high
	 * @return
	 */
	public static int getMiddle(int[] lists,int low,int high){
		int key = lists[low];
		while (low<high) {
			while (low<high&&lists[high]>=key) {
				high--;
			}
			lists[low] = lists[high];
			while(low<high&&lists[low]<=key){
				low++;
			}
			lists[high] = lists[low];
		}
		lists[low] = key;
		System.out.println("middle  "+low+"===="+key);
		return low;
	}

}
