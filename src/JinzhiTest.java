
/**
 * 十六进制简写
 * 十六进制和十进制运算
 * @author chenjianjun
 * @time 2014-10-28 下午2:50:00
 */
public class JinzhiTest {

	/**
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		//十六进制简写正确
		if (0x01==0x00000001) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		//十六进制简写错误
		if (0x01==0x01000000) {
			System.out.println(true);
		}else {
			System.out.println(false);
		}
		//十六进制和十进制的运算
		if (10==0x0a) {
			System.out.println(true);
		}else {
			System.out.println(false);
		}
		
	}

}

//true  false   true