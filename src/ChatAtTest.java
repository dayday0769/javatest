
/**
 *汉字转字节     
 *字符串转换成十进制
 *字符串转换成十六进制
 * @author chenjianjun
 * @time 2014-10-29 下午3:29:17
 */
public class ChatAtTest {
	
	public static void main(String[] args) {
		String aString = "I am 杰克.";
		convert(aString);
		String bString = "第";
		convert(bString);
		String cString = "\n";
		System.out.println(cString.length());
		convert(cString);
		
	}
	
	/**
	 * 转换
	 * @param aString
	 */
	private static void convert(String aString){
		for (int i = 0; i < aString.length(); i++) {
			char t = aString.charAt(i);
			System.out.println(t);//
			System.out.println((int)t);//ascII   十进制
			System.out.println(Integer.toHexString((int)t));//ascII   十六进制
		}
		System.out.println("======================上面是字符串转换成十进制、十六进制==============================");
		byte[] bs = null;
			bs = aString.getBytes();
		for (int i = 0; i < bs.length; i++) {
			System.out.println(bs[i]);
		}
		System.out.println("=======================上面是汉字转字节  =============================");
	}

}
