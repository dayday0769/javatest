import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SortTest {
	private static String chenString = "陈";
	private static String zhanString = "张";
	private static String liString = "李";
	private static String aString = "abd";
	private static String shuziString = "123214";
	private static String zString ="zdsf";
	 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add(chenString);
		list.add(zhanString);
		list.add(liString);
		list.add(aString);
		list.add(shuziString);
		list.add(zString);
		FileComparator fileComparator = new FileComparator();
		Collections.sort(list, fileComparator);
		for (String temp:list) {
			System.out.println(temp);
		}

	}
	
	
}
