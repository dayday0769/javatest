import java.util.ArrayList;
import java.util.List;


public class RemoveTest {
	
	public static void main(String[] args) {
		A a = new RemoveTest().new A();
		a.a = 1;
		A a1 = new RemoveTest().new A();
		a1.a = 2;
		A a2 = new RemoveTest().new A();
		a2.a = 3;
		List<A> list = new 	ArrayList<RemoveTest.A>();
		list.add(a);
		list.add(a1);
		list.add(a2);
		for (A temp:list) {
			System.out.println("main  "+temp);
		}
		removeAll(list,a,a1,a2);
		removeAll2(list);
		removeX(list,a,a1);
	}
	
	/**
	 * 移除集合list所有
	 * @param list
	 * @param a   传递过来的
	 * @param a1  传递过来的
	 * @param a2  传递过来的
	 */
	private static void removeAll(List<A> list,A a,A a1,A a2){
		List<A> list2 = new ArrayList<RemoveTest.A>();
		list2.addAll(list);
		List<A> list3 = new ArrayList<RemoveTest.A>();
		list3.add(a);
		list3.add(a1);
		list3.add(a2);
		for (A temp:list3) {
			System.out.println("removeAll  "+temp);
		}
		list3.removeAll(list2);
		System.out.println("移除传递过来的，对象引用相同的     "+list3.size());
	}
	
	/**
	 * 移除集合list所有
	 * @param list
	 * @param a  新建的
	 * @param a1  新建的
	 * @param a2  新建的
	 */
	private static void removeAll2(List<A> list){
		A a = new RemoveTest().new A();
		a.a = 1;
		A a1 = new RemoveTest().new A();
		a1.a = 2;
		A a2 = new RemoveTest().new A();
		a2.a = 3;
		List<A> list2 = new ArrayList<RemoveTest.A>();
		list2.addAll(list);
		List<A> list3 = new ArrayList<RemoveTest.A>();
		list3.add(a);
		list3.add(a1);
		list3.add(a2);
		for (A temp:list3) {
			System.out.println("removeAll2  "+temp);
		}
		list3.removeAll(list2);
		System.out.println("移除新建的，对象引用不同的     "+list3.size());
	}
	
	/**
	 * 移除2集合中相同部分
	 * @param list
	 * @param a
	 * @param a1
	 */
	private static void removeX(List<A> list,A a,A a1){
		List<A> list2 = new ArrayList<RemoveTest.A>();
		list2.addAll(list);
		List<A> list3 = new ArrayList<RemoveTest.A>();
		list3.add(a);
		list3.add(a1);
		A a4 = new RemoveTest().new A();
		a4.a = 4;
		list3.add(a4);
		list3.removeAll(list2);
		System.out.println("移除两集合中相同部分 removeX  "+list3.size());
	}
	
	class A {
		int a;
	}

}
