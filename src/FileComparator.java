import java.util.Comparator;


public class FileComparator implements Comparator<String>{

	public int compare(String lhs, String rhs) {
		
		return lhs.compareToIgnoreCase(rhs);
	}
}